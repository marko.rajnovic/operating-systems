#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <iostream>

int pid=0;

inline void prekidna_rutina(int sig){
   kill(pid, SIGKILL);
   exit(0);
}

int main(int argc, char *argv[]){
   pid=atoi(argv[1]);
   sigset(SIGINT, prekidna_rutina);
   srand(time(NULL));

   while(true){
      sleep(rand() % 3 + 3);
      int rNum = rand() % 4 + 1;

      switch(rNum){
        case 1:
          kill(pid,SIGUSR1);
          break;
        case 2:
          kill(pid,SIGUSR2);
          break;
        case 3:
          kill(pid,SIGILL);
          break;
        case 4:
          kill(pid,SIGHUP);
          break;
      }
   }
   return 0;
}
