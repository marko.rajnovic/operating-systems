#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <iostream>
#include <unistd.h>

using namespace std;

#define N 6    /* broj razina proriteta */

int OZNAKA_CEKANJA[N];
int PRIORITET[N];
int TEKUCI_PRIORITET = 0;


int sig[]={SIGUSR1, SIGUSR2, SIGILL, SIGHUP,SIGINT};
void zabrani_prekidanje(){
   int i;
   for(i=0; i<5; i++)
      sighold(sig[i]);
}
void dozvoli_prekidanje(){
   int i;
   for(i=0; i<5; i++)
      sigrelse(sig[i]);
}

void obrada_prekida(int i){
  switch(i){
    case 1:
       cout << "- " << "P" << " - - - -\n";
       break;
    case 2:
       cout << "- - " << "P" << " - - -\n";
       break;
    case 3:
       cout << "- - - " << "P" << " - -\n";
       break;
    case 4:
       cout << "- - - - " << "P" << " -\n";
       break;
    case 5:
       cout << "- - - - - " << "P" << "\n";
       break;
  }

   for(int k = 1; k <= 5; k++){
     switch(i){
       case 1:
          cout << "- " << k << " - - - -\n";
          break;
       case 2:
          cout << "- - " << k << " - - -\n";
          break;
       case 3:
          cout << "- - - " << k << " - -\n";
          break;
       case 4:
          cout << "- - - - " << k << " -\n";
          break;
       case 5:
          cout << "- - - - - " << k << "\n";
          break;
     }
     sleep(1);
   }

   switch(i){
     case 1:
        cout << "- " << "K" << " - - - -\n";
        break;
     case 2:
        cout << "- - " << "K" << " - - -\n";
        break;
     case 3:
        cout << "- - - " << "K" << " - -\n";
        break;
     case 4:
        cout << "- - - - " << "K" << " -\n";
        break;
     case 5:
        cout << "- - - - - " << "K\n";
        break;
   }
}
void prekidna_rutina(int sig){
   int n=-1;
   zabrani_prekidanje();
   switch(sig){
      case SIGUSR1:
         n=1;
         printf("- X - - - -\n");
         break;
      case SIGUSR2:
         n=2;
         printf("- - X - - -\n");
         break;
      case SIGILL:
         n=3;
         printf("- - - X - -\n");
         break;
      case SIGHUP:
          n=4;
          printf("- - - - X -\n");
          break;
      case SIGINT:
          n=5;
          printf("- - - - - X\n");
          break;
   }
   OZNAKA_CEKANJA[n] +=1;

   int x;
   do{
     x = 0;
     for(int j = TEKUCI_PRIORITET + 1; j < N; j++) {
       if(OZNAKA_CEKANJA[j] != 0){
         x = j;
        }
      }
     if(x>0){
        OZNAKA_CEKANJA[x]--;
        PRIORITET[x] = TEKUCI_PRIORITET;
        TEKUCI_PRIORITET = x;
        dozvoli_prekidanje();
        obrada_prekida(x);
        zabrani_prekidanje();
        TEKUCI_PRIORITET = PRIORITET[x];
      }
    } while(x>0);
}

void prekini_program(int sig){
  cout << "Killed";
  exit(0);
}

int main ( void )
{
   sigset (SIGUSR1, prekidna_rutina);
   sigset (SIGUSR2, prekidna_rutina);
   sigset (SIGILL, prekidna_rutina);
   sigset (SIGHUP, prekidna_rutina);
   sigset (SIGINT, prekidna_rutina);
   sigset (SIGKILL, prekini_program);

   printf("Proces obrade prekida, PID=%ld\n", getpid());
   printf("GP S1 S2 S3 S4 S5\n");
   printf("-----------------\n");
   for(int i = 1; i < 100; i++){
     cout << i << " - - - - -" << '\n';
     sleep(1);
   }

   printf ("Zavrsio osnovni program\n");

   return 0;
}
