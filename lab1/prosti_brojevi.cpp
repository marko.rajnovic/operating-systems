#include <iostream>
#include <cmath>
#include <csignal>
#include <unistd.h>
#include <sys/time.h>

using namespace std;

int prost ( unsigned long n ) {
    unsigned long i, max;

    if ( ( n & 1 ) == 0 ) /* je li paran? */
        return 0;

    max = sqrt ( n );
    for ( i = 3; i <= max; i += 2 )
        if ( ( n % i ) == 0 )
            return 0;

    return 1; /* broj je prost! */
}

int pauza = 0;
int broj = 1000000001;
int zadnji = 1000000001;

void periodicki_ispis (int signum) {
   cout << "zadnji prost broj = " << zadnji << endl;
}

void postavi_pauzu (int signum) {
   pauza = 1 - pauza;
}

void prekini (int signum) {
   cout<< "zadnji prost broj = " << zadnji << endl;
   exit(signum);
}

int main() {
   sigset(SIGINT, postavi_pauzu);
   sigset(SIGALRM, periodicki_ispis);
   sigset(SIGTERM, prekini);

   struct itimerval t;

   t.it_value.tv_sec = t.it_interval.tv_sec = 5;
	 t.it_value.tv_usec = t.it_interval.tv_usec = 0;
   setitimer(ITIMER_REAL, &t, NULL);

   while(true){
      if( prost ( broj ) == true )
         zadnji = broj;
      broj++;
      while( pauza == 1 );
   }
   return 0;
}
