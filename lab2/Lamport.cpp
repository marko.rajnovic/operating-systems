#include <iostream>
#include <thread>
#include <atomic>


using namespace std;

//sve globalne varijable definirane kao atomic
atomic<int> *ulaz, *broj;
atomic<long long> ZADNJI_BROJ{0};
atomic<long int> BROJ_DRETVI{0};
atomic<int> x{0};


//Provjerava istinitost tvrdnje (a,b) < (c,d)
bool lessThan(int a, int b, int c, int d){
	if(a < c){
		return true;
	}
	else if (a == c) {
		if(b < d){
			return true;
		}
	}
	return false;
}

void repeatAdding(int broj_ponavljanja, int i){

	//izvršavanje zbrajanja
	for(int k = 0; k < broj_ponavljanja; k++){

		//Provjera mogućnosti ulaska u k.o.
		ulaz[i].store(1);
		broj[i].store(ZADNJI_BROJ + 1);
		ZADNJI_BROJ.store(broj[i]);
		ulaz[i].store(0);
		for(int j = 0; j < BROJ_DRETVI; j++){
			atomic<int> test1;
			do{
				test1.store(ulaz[j]);
			}while(test1 == 1);

			atomic<int> test2;
			atomic<bool>  test3;
			do{
				test2.store(broj[j]);
				test3.store(lessThan(broj[j], j, broj[i], i));
			}while((test2 != 0) && test3);
		}

		//k.o.
		x.store(x+1);

		//izlaz iz k.o.
		broj[i].store(0);
	}
	return;
}

int main(int argc, char *argv[]){
	if(argc != 3){
		exit(1);
	}
	broj = new atomic<int>[atoi(argv[1])]();
	ulaz = new atomic<int>[atoi(argv[1])]();
	BROJ_DRETVI = atoi(argv[1]);

	int broj_ponavljanja = atoi(argv[2]);

	thread dretve[BROJ_DRETVI];

	//zadavanje IDa svakoj dretvi
	int kljuc_dretve[BROJ_DRETVI];
	for(int i = 0; i < BROJ_DRETVI; i++){
		kljuc_dretve[i] = i;
	}

	//inicijaliziranje BROJ_DRETVI dretvi
	for(int i = 0; i < BROJ_DRETVI; i++){
		dretve[i] = thread(repeatAdding, broj_ponavljanja, kljuc_dretve[i]);
	}

	//čekanje kraja svih dretvi
	for(int i = 0; i < BROJ_DRETVI; i++){
		dretve[i].join();
	}

	cout << x << endl;
}
