#include <thread>
#include <iostream>

using namespace std;

int x = 0;

void repeatAdding(int broj_ponavljanja){
    for(int i = 0; i < broj_ponavljanja; i++){
        ++x;
    }
}


int main(int argc, char *argv[]){
    if(argc != 3){
        exit(1);
    }
    int broj_dretvi = atoi(argv[1]);
    int broj_ponavljanja = atoi(argv[2]);

    thread dretve[broj_dretvi];
    for(int i = 0; i < broj_dretvi; i++){
        dretve[i] = thread(repeatAdding, broj_ponavljanja);
    }

    for(int i = 0; i < broj_dretvi; i++){
        dretve[i].join();
    }

    cout << "A=" <<x << endl;
}
