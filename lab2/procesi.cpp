#include <unistd.h>
#include <iostream>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>


using namespace std;

int x = 0;

void repeatAdding(int broj_ponavljanja){
    for(int i = 0; i < broj_ponavljanja; i++){
        ++x;
    }
}


int main(int argc, char *argv[]){
    if(argc != 3){
        exit(1);
    }
    int broj_procesa = atoi(argv[1]);
    int broj_ponavljanja = atoi(argv[2]);


    for(int i = 0; i < broj_procesa; i++){
        if(fork() == 0){
			repeatAdding(broj_ponavljanja);
			exit(0);
		}
    }

	repeatAdding(broj_ponavljanja);
	for (int i=0; i < broj_procesa; i++)
		wait(NULL);
	cout << "A=" << x << endl;
}
