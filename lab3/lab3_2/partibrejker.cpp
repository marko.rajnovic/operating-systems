#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <vector>
#include <pthread.h>

//NAPOMENA: "g++ -std=c++0x -pthread -g partibrejker.cpp" dobro kompajlira program

//globalne varijable
int BROJ_STUDENATA;
bool partibrejkerUSobi = false;
int brojLjudiUSobi = 0;

//monitor
pthread_mutex_t m;
pthread_cond_t red[2];



class Student{

    int m_zeliPartijati = 3;
    int m_brojStudenta;

public:
    Student(int brojStudenta){
        m_brojStudenta = brojStudenta;
    }

    bool zeliLiStudentJosPartijati(){
        if(m_zeliPartijati > 0){
            return true;
        }
        return false;
    }

    //ispisuje da je student ušao, smanjuje učenikovu "energiju" i pocećava broj ljudi u sobi
    void udjiUSobu(){
        std::cout << "Student " << m_brojStudenta << " je ušao u sobu\n";
        --m_zeliPartijati;
        ++brojLjudiUSobi;
    }

    void zabaviSe(){
        usleep((rand() % 1000 + 1000)*1000);
    };

    //ispisuje da student izlazi i smanjuje broj ljudi u sobi
    void izadjiIzSobe(){
        std::cout << "Student " << m_brojStudenta << " je izašao iz sobe\n";
        --brojLjudiUSobi;
    }

    int getBrojStudenta(){
        return m_brojStudenta;
    }

    ~Student();
};


class Partibrejker{
public:
    void udjiUsobu(){
        std::cout << "Partibrejker je ušao u sobu\n";
        partibrejkerUSobi = true;
    }

    void izadjiIzSobe(){
        std::cout << "Partibrejker je izašao iz sobe\n";
        partibrejkerUSobi = false;
    }

    ~Partibrejker();
};


void *studentFunction(void* stud2){
    //casta studenta iz voida nazad u Student*
    Student* stud = static_cast<Student*>(stud2);

    //student miruje
    usleep((rand() % 400 + 100) * 1000);

    //ako student želi još partijati
    while(stud->zeliLiStudentJosPartijati()){
        pthread_mutex_lock(&m);

        //ako je partibrejker u sobi
        while(partibrejkerUSobi){
            //čekaj dok ne izađe
            pthread_cond_wait(&red[0], &m);
        }

        stud->udjiUSobu();
        //ako je više od troje ljudi u sobi
        if(brojLjudiUSobi >= 3){
            //oslobodi partibrejkera da može ući
            pthread_cond_signal(&red[1]);
        }
        pthread_mutex_unlock(&m);

        stud->zabaviSe();

        pthread_mutex_lock(&m);
        stud->izadjiIzSobe();
        //ako nema više nikoga u sobi
        if(brojLjudiUSobi == 0){
            //oslobodi partibrejkera da može izaći
            pthread_cond_signal(&red[1]);
        }
        pthread_mutex_unlock(&m);

        //student miruje
        usleep((rand() % 1000 + 1000)*1000);
    }
    pthread_mutex_lock(&m);
    --BROJ_STUDENATA;

    //ako su svi ošli doma;
    if(BROJ_STUDENATA == 0){
        //omogući da i partibrejker ode doma ako je negdje zaglavio
        pthread_cond_signal(&red[1]);
    }
    pthread_mutex_unlock(&m);
    return 0;
}

void* partibrejkerFunction(void *part2){
    //casta partibrejkera iz voida nazad u Partibrejker*
    Partibrejker* part = static_cast<Partibrejker*>(part2);

    //dok nisu svi ošli kući
    while(BROJ_STUDENATA != 0){
            usleep((rand() % 900 + 100) * 1000);

            pthread_mutex_lock(&m);
            //dok nisu svi ošli kući i ima <3 studenta u sobi
            while(BROJ_STUDENATA != 0 && brojLjudiUSobi < 3){
                //čekaj
                pthread_cond_wait(&red[1], &m);
            }
            if(BROJ_STUDENATA == 0){
                break;
            }
            pthread_mutex_unlock(&m);

            pthread_mutex_lock(&m);
            part->udjiUsobu();
            //ako ima ljudi u sobi
            while(brojLjudiUSobi > 0){
                //čekaj dok te zadnji ne probudi da odeš
                pthread_cond_wait(&red[1], &m);
            }

            part->izadjiIzSobe();

            //kada izadjes, probudi sve koji čekaju
            pthread_cond_broadcast(&red[0]);
            pthread_mutex_unlock(&m);

    }
    return 0;
}

int main()
{
    std::cout << "Unesite broj studenata koji će biti na partiju: ";
    std::cin >> BROJ_STUDENATA;
    int POCETNI_BROJ_LJUDI = BROJ_STUDENATA;

    pthread_t t[POCETNI_BROJ_LJUDI + 1];

    srand(time(NULL));

    //inicijalizacija monitora
    pthread_mutex_init (&m, NULL);
    pthread_cond_init (&red[0], NULL);
    pthread_cond_init (&red[1], NULL);

    //inicijalizacija studenata i partibrejkera
    Student **studenti = new Student*[POCETNI_BROJ_LJUDI];
    for(int i = 0; i < POCETNI_BROJ_LJUDI; i++){
        studenti[i] = new Student(i);
    }
    Partibrejker *part = new Partibrejker();


    //stvaranje threadova
    for(int i = 0; i < POCETNI_BROJ_LJUDI; i++){
        pthread_create(&t[i], NULL, studentFunction, (void *)studenti[i]);
    }
    pthread_create(&t[POCETNI_BROJ_LJUDI], NULL, partibrejkerFunction, (void *)part);

    //čekanje na završetak threadova
    for (int i = 0; i <= POCETNI_BROJ_LJUDI; i++){
        pthread_join (t[i], NULL);
        //std::cout << "trenutno se čeka:" << i << "\n";
    }

    //brisanje monitora
    pthread_mutex_destroy (&m);
    pthread_cond_destroy (&red[0]);
    pthread_cond_destroy (&red[1]);

}
