#include <stdio.h>
#include <thread>
#include <semaphore.h>
#include <stdlib.h>
#include <iostream>
#include <unistd.h>

//NAPOMENA:: "g++ -std=c++0x -pthread -g pusaci.cpp" dobro kompajlira program



//semafori
sem_t KO;
sem_t s1;
sem_t s2;
sem_t s3;
sem_t s[3] = {s1,s2,s3};

//globalni stol
int *stol = new int[3];


int BROJ_PUSACA = 3;

//Simulira trgovca koji stavlja predmete na stol
class Trgovac{
public:
    Trgovac(){
        srand(time(NULL));
    }

    int m_moguceDodjele[3][3] = {{0,1,1},{1,0,1},{1,1,0}};

    void stavi_na_stol(){
        int randomNumber = rand() % 3;
        stol = m_moguceDodjele[randomNumber];

        std::cout << "Trgovac na stol stavlja: ";

        if(stol[0] == 0){
            std::cout << "duhan i šibice\n";
        }
        if(stol[1] == 0){
            std::cout << "duhan i papir\n";
        }
        if(stol[2] == 0){
            std::cout << "šibice i papir\n";
        }

    }
};


//simulira pušača koji uzima predmete sa stola
class Pusac{
    //0: Pušač posjeduje papir
    //1: Pušač posjeduje duhan
    //2: Pušač posjeduje šibice
    int m_sredstvo;

public:
    Pusac(int sredstvo){
        m_sredstvo = sredstvo;
    }

    void smotajZapaliPusi(){
        sleep(1);
        std::cout << "Pušač " << m_sredstvo + 1 << " puši\n";
        sleep(4);
    }

    bool naStoluPotrebanPribor(){
        if(stol != nullptr && stol[m_sredstvo] == 0){
            sleep(1);
            std::cout << "Pušač " << m_sredstvo + 1 << " uzima: ";
            if(stol[0] == 0){
                std::cout << "duhan i šibice\n";
            }
            if(stol[1] == 0){
                std::cout << "duhan i papir\n";
            }
            if(stol[2] == 0){
                std::cout << "šibice i papir\n";
            }

            stol = nullptr;
            return true;

            }
        return false;
    }

    int getSredstvo(){
        return m_sredstvo;
    }
};


//funkcija koja kontrolira svakog pušača (nisam stavio ovo u objekt pusaca jer mi je bilo prekomplicirano)
void pusacFunction(Pusac *pus){
    while(1){
        sem_wait(&s[pus->getSredstvo()]);
        if(pus->naStoluPotrebanPribor()){
            sem_post(&KO);
            pus->smotajZapaliPusi();
        }
    }
}

//funkcija koja kontrolira trgovca (nisam stavio ovo u objekt trgovca jer mi je bilo prekomplicirano)
void trgovacFunction(Trgovac *trg){
    while(1){
        sem_wait(&KO);
        trg->stavi_na_stol();
        for(int i = 0; i < BROJ_PUSACA; i++){
            sem_post(&s[i]);
        }
        sleep(1);
    }
}

int main(){
    //inicijalizacija semafora
    sem_init(&KO, 0, 1);
    sem_init(&s[0], 0, 0);
    sem_init(&s[1], 0, 0);
    sem_init(&s[2], 0, 0);

    //inicijalizacija pusaca
    Pusac *pus1 = new Pusac(0);
    Pusac *pus2 = new Pusac(1);
    Pusac *pus3 = new Pusac(2);

    //inicijalizacija trgovaca
    Trgovac *trg = new Trgovac();

    //pokretanje beskonačnih dretvi
    std::thread t1(trgovacFunction, trg);
    std::thread t2(pusacFunction, pus1);
    std::thread t3(pusacFunction, pus2);
    std::thread t4(pusacFunction, pus3);

    //nikada se neće t1 joinat, ovo je samo za sprječavanje završetka maina
    t1.join();
}
