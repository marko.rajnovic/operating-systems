#include <iostream>
#include <algorithm>
#include <vector>
#include <unistd.h>
#include <stdio.h>

//NAPOMENA: "g++ -std=c++0x -g visekriterijsko.cpp" dobro kompajlira program

using namespace std;

int DRETVI = 6;
int MAX_DRETVI = 5;

class Dretva{
public:
    int tDolaska;
    int id;
    int tRada;
    int prio;
    int rasp;
    bool odradjenaSad = false;

    Dretva(int tDolaska, int id, int tRada, int prio, int rasp = 0){
        this->tDolaska = tDolaska;
        this->id = id;
        this->tRada = tRada;
        this->prio = prio;

        // 0 -> PRIO + FIFO; 1-> PRIO + RR
        this->rasp = rasp;
    }

    Dretva(){};

    void napraviPosao(){
        --tRada;
        odradjenaSad = true;
    }

    bool dretvaGotova(){
        if (tRada == 0){
            return true;
        }
        return false;
    }

    void resetiraj(){
        this->odradjenaSad = false;
    }

    friend bool operator< (const Dretva &d1, const Dretva &d2);
    friend bool operator> (const Dretva &d1, const Dretva &d2);
    friend bool operator== (const Dretva &d1, const Dretva &d2);
};

bool operator< (const Dretva &d1, const Dretva &d2){
    return d1.prio < d2.prio;
}

bool operator> (const Dretva &d1, const Dretva &d2){
    return d1.prio > d2.prio;
}

bool operator== (const Dretva &d1, const Dretva &d2){
    return d1.prio == d2.prio;
}

void pocetniIspis(){
    printf ( "  t    AKT" );
    for (int i = 1; i < MAX_DRETVI; i++ )
        printf ( "     PR%d", i );
    printf ( "\n" );
}

void ispisiStanje(vector<Dretva> &trenutnoAktivneDretve, int t){
    int counter=-1;

    printf( "%3d ",t);
    for(unsigned long i = 0; i < trenutnoAktivneDretve.size(); i++){
        cout <<"  " << trenutnoAktivneDretve[i].id << "/" << trenutnoAktivneDretve[i].prio << "/" << trenutnoAktivneDretve[i].tRada <<" ";
        counter = i;
    }

    while(counter < MAX_DRETVI - 1){
        cout << "  -/-/- ";
        counter++;
    }
    cout << "\n";
}

int main(){
    vector<Dretva> dretve;
    dretve.push_back(Dretva(1,3,5,3,1));
    dretve.push_back(Dretva(3,5,6,5,1));
    dretve.push_back(Dretva(7,2,3,5,0));
    dretve.push_back(Dretva(12,1,5,3,0));
    dretve.push_back(Dretva(20,6,3,6,1));
    dretve.push_back(Dretva(20,7,4,7,1));

    int t = 0;

    vector<Dretva> trenutnoAktivneDretve;
    pocetniIspis();
    //dretve poredane po prioritetima

    Dretva dretva;
    while(true){

        if(!trenutnoAktivneDretve.empty()){
            trenutnoAktivneDretve[0].napraviPosao();
            if(trenutnoAktivneDretve[0].tRada == 0){
                cout << "Dretva " << trenutnoAktivneDretve[0].id << " završila\n";
                trenutnoAktivneDretve.erase(trenutnoAktivneDretve.begin());
            }
            //ako se dretva već izvršila i ako se raspoređuje prema RR i ako je ona iza nje istog prioriteta, neka se poput mjehurića ova prva dretva stavi na zadnje mjesto iza svih dretvi svog prioriteta
            if(trenutnoAktivneDretve.size() > 1 && trenutnoAktivneDretve[0].rasp == 1 && trenutnoAktivneDretve[0].odradjenaSad && trenutnoAktivneDretve[0] == trenutnoAktivneDretve[1]){
                int counter = 0;
                trenutnoAktivneDretve[0].resetiraj();
                while(counter < trenutnoAktivneDretve.size() - 1 && trenutnoAktivneDretve[counter] == trenutnoAktivneDretve[counter + 1]){
                    std::swap(trenutnoAktivneDretve[counter],trenutnoAktivneDretve[counter + 1]);
                    counter++;
                }
            }
            trenutnoAktivneDretve[0].resetiraj();
        }

        ispisiStanje(trenutnoAktivneDretve, t);

        //provjeri dolazi li koja nova dretva;
        for(unsigned long i = 0; i < dretve.size(); i++){
            if(dretve[i].tDolaska == t){
                dretva = dretve[i];
                trenutnoAktivneDretve.push_back(dretva);
                printf("%3d -- nova dretva id=%d, p=%d, prio=%d\n", t, dretva.id, dretva.tRada, dretva.prio);
                stable_sort(trenutnoAktivneDretve.begin(),trenutnoAktivneDretve.end(), greater<Dretva>());
                ispisiStanje(trenutnoAktivneDretve,t);
            }
        }


        ++t;
        sleep(1);
    }

    return 0;
}
